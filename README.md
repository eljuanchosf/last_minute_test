# Last Minute Test

## Requirements

* [Ruby 2.7.0](https://www.ruby-lang.org/en/downloads/)

### Recommendations

* [RVM](https://rvm.io/) to manage Ruby versions


## Running it

1. Once you have Ruby 2.7.0 working in your system, from the root directory of the project run:

        $ gem install bundler:2.1.4
        $ bundle
2. Due to the nature of the project, there is no "input" mechanism. There is only the possibility of running the specs, which you can find in the homonimous directory.

        $ bundle exec rspec
3. Alternatively, you can run the pipeline from [GitLab's CI/CD tool](https://gitlab.com/eljuanchosf/last_minute_test/pipelines).