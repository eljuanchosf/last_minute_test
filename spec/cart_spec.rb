# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/taxator'
require_relative '../lib/cart'

describe Cart do
  let(:products) { JSON.parse(
    YAML.load_file('spec/fixtures/products.yml').to_json, 
    object_class: OpenStruct)
  }
  let(:cart) { Cart.new }

  it 'returns correct taxes and total price for local products' do
    cart.add(find_product('book'))
    cart.add(find_product('cd'))
    cart.add(find_product('chocolate_bar'))
    cart.checkout
    expect(cart.collected_tax).to eq(BigDecimal('1.5'))
    expect(cart.total_price).to eq(BigDecimal('29.83'))
  end

  it 'returns correct taxes and total price for imported products' do
    cart.add(find_product('imported_box_of_chocolates'))
    cart.add(find_product('imported_bottle_of_perfume'))
    cart.checkout
    expect(cart.collected_tax).to eq(BigDecimal('7.65'))
    expect(cart.total_price).to eq(BigDecimal('65.15'))
  end

  it 'returns correct taxes and total price for mixed products' do
    cart.add(find_product('imported_bottle_of_perfume_2'))
    cart.add(find_product('bottle_of_perfume'))
    cart.add(find_product('packet_of_headache_pills'))
    cart.add(find_product('box_of_imported_chocolates'))
    cart.checkout
    expect(cart.collected_tax).to eq(BigDecimal('6.7'))
    expect(cart.total_price).to eq(BigDecimal('74.68'))
  end
end
