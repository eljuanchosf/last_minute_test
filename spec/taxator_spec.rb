# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/taxator'

describe Taxator do
  let(:products) { JSON.parse(
    YAML.load_file('spec/fixtures/products.yml').to_json, 
    object_class: OpenStruct)
  }

  context 'sales tax' do
    it 'apply tax to a non-exempt product' do
      taxator = Taxator.new(find_product('cd')).apply_sales_tax
      expect(taxator.collected_tax).to eq(BigDecimal('1.5'))
    end

    it 'does not apply tax to an exempt product' do
      taxator = Taxator.new(find_product('book')).apply_sales_tax
      expect(taxator.collected_tax).to eq(BigDecimal('0.0'))
    end
  end

  context 'import tax' do
    it 'applies to an imported product' do
      taxator = Taxator.new(find_product('imported_bottle_of_perfume')).apply_imports_tax
      expect(taxator.collected_tax).to eq(BigDecimal('2.4'))
    end

    it 'does not apply to a local product' do
      taxator = Taxator.new(find_product('book')).apply_imports_tax
      expect(taxator.collected_tax).to eq(BigDecimal('0.0'))
    end
  end

  context 'all taxes' do
    it 'applies taxes to a imported, non exempt product' do
      taxator = Taxator.new(find_product('imported_bottle_of_perfume')).apply_all_taxes
      expect(taxator.collected_tax).to eq(BigDecimal('7.15'))
    end

    it 'applies taxes to a non imported, non exempt product' do
      taxator = Taxator.new(find_product('cd')).apply_all_taxes
      expect(taxator.collected_tax).to eq(BigDecimal('1.5'))
    end

    it 'applies taxes to a imported, exempt product' do
      taxator = Taxator.new(find_product('box_of_imported_chocolates')).apply_all_taxes
      expect(taxator.collected_tax).to eq(BigDecimal('0.6'))
    end

    it 'does not apply taxes to a non imported, exempt product' do
      taxator = Taxator.new(find_product('book')).apply_all_taxes
      expect(taxator.collected_tax).to eq(BigDecimal('0.0'))
    end
  end
end
