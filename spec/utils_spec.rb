# frozen_string_literal: true

require 'spec_helper'

require_relative '../lib/utils'

describe Utils do
  let(:utils) { Class.new { extend Utils } }

  context 'round up' do
    it 'rounds up an amount to the nearest 0.05' do
      expect(utils.round_up(BigDecimal('1.127'))).to eq(BigDecimal('1.15'))
    end

    it 'does not round single decimal digits' do
      expect(utils.round_up(BigDecimal('6.7'))).to eq(BigDecimal('6.7'))
    end
  end
  context 'formatter' do
    it 'formats a BigDecimal precision with 3 decimals to two decimals' do
      expect(utils.format_amount(BigDecimal('123.1234'))).to eq('123.12')
    end
  end
end
