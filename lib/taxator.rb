# frozen_string_literal: true

require 'bigdecimal'
require 'utils'

# Taxator is a service object that applies taxes to products according to rules
# defined in each tax method.
class Taxator
  include Utils

  attr_accessor :product, :collected_tax

  def initialize(product)
    @product = product
    @collected_tax = BigDecimal('0.00')
  end

  def apply_all_taxes
    apply_sales_tax
    apply_imports_tax
  end

  def apply_sales_tax
    exempt_categories = %w[books food medical]
    tax_rate = BigDecimal('10.00')
    unless exempt_categories.include?(@product.category)
      @collected_tax += apply_tax(@product.price, tax_rate)
    end
    self
  end

  def apply_imports_tax
    tax_rate = BigDecimal('5.00')
    @collected_tax += apply_tax(@product.price, tax_rate) if @product.imported
    self
  end

  private

  def apply_tax(price, tax_rate)
    round_up(BigDecimal(price) * (BigDecimal(tax_rate) / 100))
  end
end
