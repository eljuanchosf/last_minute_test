# frozen_string_literal: true

module Utils
  def round_up(amount)
    round_to = BigDecimal('0.05')
    ((amount / round_to).ceil * round_to).round(2)
  end

  def format_amount(amount)
    '%.2f' % amount.truncate(2).to_s('F')
  end
end
