# frozen_string_literal: true

require 'taxator'
require 'utils'

# Cart represents a very simple shopping cart implementation
class Cart
  include Utils

  attr_accessor :products, :collected_tax, :total_price

  def initialize
    @products      = []
    @collected_tax = BigDecimal('0.0')
    @total_price   = BigDecimal('0.0')
  end

  def add(product)
    @products << product
  end

  def checkout
    print_cart
    @products.each do |product|
      product_tax = get_taxes(product)
      @collected_tax += product_tax
      @total_price += BigDecimal(product.price)
      product.price_after_taxes = format_amount(BigDecimal(product.price) +
                                  product_tax)
    end
    @total_price = (@total_price + @collected_tax)
    print_checkout
  end

  private

  def get_taxes(product)
    taxator = Taxator.new(product).apply_all_taxes
    taxator.collected_tax
  end

  def print_cart
    print "Cart contents\n"
    @products.each do |product|
      puts "1 #{product.name} at #{product.price}"
    end
  end

  def print_checkout
    puts "\nCart Checkout"
    @products.each do |product|
      puts "1 #{product.name}: #{product.price_after_taxes}"
    end
    puts "Sales tax: #{format_amount(@collected_tax)}"
    puts "Total: #{format_amount(@total_price)}\n\n"
  end
end
